import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:simple_portfolio/time_line/TimeLineSteps.dart';
import 'package:simple_portfolio/time_line/DummyStep.dart';
import 'package:simple_portfolio/Header.dart';
import 'package:simple_portfolio/widgets/chips.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<DummyStep> _dummyEducation;
  List<DummyStep> _dummyWorkExperience;

  @override
  void initState() {
    _dummyEducation = _generateDummyEducation();
    _dummyWorkExperience = _generateDummyWorkExperience();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Theme(
          data: Theme.of(context).copyWith(
            accentColor: const Color(0xFFFCB69F).withOpacity(0.2),
          ),
          child: Scaffold(
              backgroundColor: Colors.transparent,
              body: ListView(
                children: <Widget>[
                  Header(),
                  Padding(
                    padding:
                        EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0),
                    child: new RichText(
                      text: new TextSpan(
                        // Note: Styles for TextSpans must be explicitly defined.
                        // Child text spans will inherit styles from parent
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(
                              text: 'Hello I\'m ',
                              style: new TextStyle(
                                fontFamily: 'dunno',
                                fontWeight: FontWeight.w100,
                                fontSize: 40,
                              )),
                          new TextSpan(
                              text: 'Kamyab Alex',
                              style: new TextStyle(
                                  fontFamily: 'dunno',
                                  fontWeight: FontWeight.w500,
                                  fontSize: 40,
                                  color: Color(0xFF55CCBF))),
                        ],
                      ),
                    ),
                  ),
                  Divider(indent: 10.0, endIndent: 10.0, color: Colors.black),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '
                        'Nullam convallis porta lacus, eu euismod turpis lobortis'
                        ' quis. Aliquam egestas enim ipsum, ut eleifend mi ullamcorper '
                        'sit amet. Aenean sit amet accumsan mauris. Mauris vel sapien '
                        'erat. Aliquam quam nunc, blandit ut nibh vitae, auctor efficitur'
                        ' dui. Fusce ut lacus sed arcu dictum ullamcorper. Vivamus pulvinar,'
                        ' diam eget mattis efficitur, quam magna sodales justo, tempus porttitor'
                        ' nisi est vitae mi. Sed euismod pretium ipsum, in molestie magna '
                        'malesuada a. Nulla varius a nunc quis consequat.',
                        style: new TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 20,
                            fontWeight: FontWeight.w300)),
                  ),
                  SkillsChips(),
                  LanguageChips(),
                  TitleWithDivider(
                    title: "Education",
                  ),
                  TimelineSteps(steps: _dummyEducation),
                  TitleWithDivider(
                    title: "Work Experience",
                  ),
                  TimelineSteps(steps: _dummyWorkExperience),
                ],
              )),
        ),
      ),
    );
  }

  List<DummyStep> _generateDummyEducation() {
    return <DummyStep>[
      const DummyStep(
          step: 1,
          at: "1994-06 - 2005 - 09",
          title: 'Chalmers Campus Lindholmen',
          message: 'Lorem ipsum dolor sit amet, consectetur adipiscing'
              ' elit, sed do eiusmod tempor incididunt ut labore'
              ' et dolore magna aliqua. Mi ipsum faucibus vitae '
              'aliquet nec ullamcorper sit. Quis auctor elit sed '),
      const DummyStep(
          step: 2,
          at: "2010-05 - 2016-05",
          title: 'Goteborgs universitet',
          message: 'Eget gravida cum sociis natoque penatibus. Est pellentesque'
              ' elit ullamcorper dignissim. Ridiculus mus mauris vitae'
              ' ultricies leo integer malesuada nunc. Malesuada fames '
              'ac turpis egestas maecenas. Malesuada fames ac turpis'
              ' egestas sed tempus urna et. Proin sagittis nisl rhoncus'),
      const DummyStep(
          step: 3,
          at: "2010-05 - Present",
          title: 'Stockholm University',
          message: 'Eget gravida cum sociis natoque penatibus. Est pellentesque'
              ' elit ullamcorper dignissim. Ridiculus mus mauris vitae'
              ' ultricies leo integer malesuada nunc. Malesuada fames '
              'ac turpis egestas maecenas. Malesuada fames ac turpis'
              ' egestas sed tempus urna et. Proin sagittis nisl rhoncus')
    ];
  }

  List<DummyStep> _generateDummyWorkExperience() {
    return <DummyStep>[
      const DummyStep(
          step: 1,
          at: "At Licenceed AB",
          title: 'Web Devloper',
          message: 'Lorem ipsum dolor sit amet, consectetur adipiscing'
              ' elit, sed do eiusmod tempor incididunt ut labore'
              ' et dolore magna aliqua. Mi ipsum faucibus vitae '
              'aliquet nec ullamcorper sit. Quis auctor elit sed '),
      const DummyStep(
          step: 2,
          at: "At TakenJob",
          title: 'Back-End Developer',
          message: 'Eget gravida cum sociis natoque penatibus. Est pellentesque'
              ' elit ullamcorper dignissim. Ridiculus mus mauris vitae'
              ' ultricies leo integer malesuada nunc. Malesuada fames '),
    ];
  }
}

class TitleWithDivider extends StatelessWidget {
  const TitleWithDivider({Key key, @required this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Divider(indent: 10.0, endIndent: 10.0, color: Colors.black),
        Padding(
          padding: EdgeInsets.only(left: 10.0),
          child: Text(title,
              style: TextStyle(
                  color: Colors.grey[800],
                  fontFamily: 'Lato',
                  fontWeight: FontWeight.bold,
                  fontSize: 40)),
        ),
        Divider(indent: 10.0, endIndent: 10.0, color: Colors.black),
      ],
    );
  }
}

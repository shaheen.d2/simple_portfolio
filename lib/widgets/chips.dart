import 'package:flutter/material.dart';
class SkillsChips extends StatelessWidget {
  const SkillsChips({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Wrap(
        spacing: 5.0,
        children: <Widget>[
          SkillChip("My Sql", "M"),
          SkillChip("Cake PHP", "C"),
          SkillChip("Javascript", "JS"),
          SkillChip("Laravel", "LA"),
        ],
      ),
    );
  }
}

class SkillChip extends StatelessWidget {
  final String name;
  final String letters;
  const SkillChip(this.name, this.letters);

  @override
  Widget build(BuildContext context) {
    return Chip(
      avatar: CircleAvatar(
        backgroundColor: Colors.grey.shade800,
        child: Text(letters),
      ),
      backgroundColor: Color(0xFF9B73E9),
      label: Text(name,
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Lato',
            fontWeight: FontWeight.w500,
          )),
    );
  }
}

class LanguageChips extends StatelessWidget {
  const LanguageChips({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 20.0),
      child: Wrap(
        spacing: 5.0,
        children: <Widget>[
          LanguageChip('English'),
          LanguageChip('Persian'),
          LanguageChip('Arabic'),

        ],
      ),
    );
  }
}

class LanguageChip extends StatelessWidget {
  final String title;
  const LanguageChip(this.title);

  @override
  Widget build(BuildContext context) {
    return Chip(
      backgroundColor: Color(0xFF4393D4),
      label: Text(title,
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Lato',
            fontWeight: FontWeight.w500,
          )),
    );
  }
}

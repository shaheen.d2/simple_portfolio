import 'package:flutter/material.dart';

class TimelineStepsChild extends StatelessWidget {
  const TimelineStepsChild(
      {Key key, this.title, this.subtitle, this.isLeftAlign, this.at})
      : super(key: key);

  final String title;
  final String subtitle;
  final String at;
  final bool isLeftAlign;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: isLeftAlign
          ? const EdgeInsets.only(right: 32, top: 16, bottom: 16, left: 10)
          : const EdgeInsets.only(left: 32, top: 16, bottom: 16, right: 10),
      child: Column(
        crossAxisAlignment:
            isLeftAlign ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TimeLineText(title: title, isLeftAlign: isLeftAlign),
          TimeLineText(
            title: at,
            isLeftAlign: isLeftAlign,
            textColor: 0xFF55CCBF,
          ),
          const SizedBox(height: 5),
          TimeLineText(title: subtitle, isLeftAlign: isLeftAlign,weight: FontWeight.w200,),
        ],
      ),
    );
  }
}

class TimeLineText extends StatelessWidget {
  const TimeLineText(
      {Key key,
      @required this.title,
      @required this.isLeftAlign,
      this.textColor,
      this.weight})
      : super(key: key);

  final String title;
  final bool isLeftAlign;
  final int textColor;
  final FontWeight weight;

  @override
  Widget build(BuildContext context) {
    return Text(title,
        textAlign: isLeftAlign ? TextAlign.right : TextAlign.left,
        style: new TextStyle(
            fontFamily: 'Lato',
            fontSize: 18,
            color: (textColor != null ? new Color(textColor) : Colors.black),
            fontWeight: (weight !=null ? weight : FontWeight.w500)));
  }
}

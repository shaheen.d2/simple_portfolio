import 'dart:math';

import 'package:flutter/material.dart';
import 'package:timeline_tile/timeline_tile.dart';
import 'package:simple_portfolio/time_line/TimeLineStepIndicator.dart';
import 'package:simple_portfolio/time_line/TimeLineStepsChild.dart';
import 'package:simple_portfolio/time_line/DummyStep.dart';

class TimelineSteps extends StatelessWidget {
  const TimelineSteps({Key key, this.steps}) : super(key: key);

  final List<DummyStep> steps;

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemCount: max(0, steps.length * 2 - 1),
      itemBuilder: (BuildContext context, int index) {
        if (index.isOdd)
          return const TimelineDivider(
            color: Color(0xFF2D3E50),
            thickness: 3,
            begin: 0.1,
            end: 0.9,
          );

        final int itemIndex = index ~/ 2;
        final DummyStep step = steps[itemIndex];

        final bool isLeftAlign = itemIndex.isEven;

        final child = TimelineStepsChild(
          at: step.at,
          title: step.title,
          subtitle: step.message,
          isLeftAlign: isLeftAlign,
        );

        final isFirst = itemIndex == 0;
        final isLast = itemIndex == steps.length - 1;
        double indicatorY;
        if (isFirst) {
          indicatorY = 0.2;
        } else if (isLast) {
          indicatorY = 0.8;
        } else {
          indicatorY = 0.5;
        }

        return TimelineTile(
          alignment: TimelineAlign.manual,
          rightChild: isLeftAlign ? child : null,
          leftChild: isLeftAlign ? null : child,
          lineX: isLeftAlign ? 0.1 : 0.9,
          isFirst: isFirst,
          isLast: isLast,
          indicatorStyle: IndicatorStyle(
            width: 40,
            height: 40,
            indicatorY: indicatorY,
            indicator: TimelineStepIndicator(step: '${step.step}'),
          ),
          topLineStyle: const LineStyle(

            color: Color(0xFF2D3E50),
            width: 3,
          ),
        );
      },
    );
  }
}

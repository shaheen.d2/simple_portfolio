class DummyStep {
  const DummyStep({
    this.step,
    this.title,
    this.at,
    this.message,
  });

  final int step;
  final String title;
  final String message;
  final String at;
}
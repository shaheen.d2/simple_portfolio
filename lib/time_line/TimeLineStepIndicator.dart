import 'package:flutter/material.dart';

class TimelineStepIndicator extends StatelessWidget {
  const TimelineStepIndicator({Key key, this.step}) : super(key: key);

  final String step;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
        color: Color(0xFF2F485E),
      ),
      child: Center(
        child: Text(step,
            style: TextStyle(
                color: Colors.white,),
        ),
      ),
    );
  }
}
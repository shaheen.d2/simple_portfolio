import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Stack(
      overflow: Overflow.visible,
      alignment: Alignment.center,
      children: <Widget>[
        Positioned(
          width: 700.0,
          top: 0.0,
          bottom: -(MediaQuery.of(context).size.height / 3),
          child: ClipPath(
            child: Container(color: Color(0xFF2D3E50)),
            clipper: getClipper(),
          ),
        ),
        Container(
            margin:
                EdgeInsets.only(top: MediaQuery.of(context).size.height / 15),
            width: 150.0,
            height: 150.0,
            decoration: BoxDecoration(
              border: new Border.all(width: 3.0,color: Color(0xFF55CCBF) ),
                color: Color(0xFF55CCBF),
                image: DecorationImage(
                    image: Image.asset('images/pro.png').image,
                    fit: BoxFit.cover),
                borderRadius: BorderRadius.all(Radius.circular(75.0)),
                boxShadow: [BoxShadow(blurRadius: 7.0, color: Colors.black)])),
      ],
    );
  }
}

class getClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();

    path.lineTo(0.0, size.height / 1.9);
    path.lineTo(size.width + 125, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
